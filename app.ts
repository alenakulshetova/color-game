// elements
let numberOfSquaresSelect = document.getElementById("mode") as HTMLSelectElement;
let squaresContainer = document.getElementById("container");
let targetColorTitle = document.getElementById("colorDisplay");
let messageContainer = document.getElementById("message");
let resetButton = document.getElementById("reset");

// colors generator
function generateRandomColors(numb: number): Array<string> {
    let arr = []
    for (let i = 0; i < numb; i++) {
        let r = Math.floor(Math.random() * 256);
        let g = Math.floor(Math.random() * 256);
        let b = Math.floor(Math.random() * 256);
        arr.push("rgb(" + r + ", " + g + ", " + b + ")");
    }
    return arr;
}

function pickTargetColor(colors: Array<string>): string {
    let random = Math.floor(Math.random() * colors.length);
    return colors[random];
}

// application state
interface AplicationState {
    numberOfSquares: number
    colors: Array<string>
    targetColor: string
    squares: Array<HTMLDivElement>
    won: boolean
}

const aplicationState: AplicationState = {
    numberOfSquares: 3,
    colors: new Array<string>(),
    targetColor: '',
    squares: new Array<HTMLDivElement>(),
    won: false
}

function handleSquareClick(square: HTMLDivElement, color: string) {
    if (!aplicationState.won) {
        if (aplicationState.targetColor === color) {
            aplicationState.won = true

            // win
            messageContainer.textContent = "You are good at guessing!";
            resetButton.textContent = "Play Again?"

            // set target color to all squares
            for (let i = 0; i < aplicationState.squares.length; i++) {
                aplicationState.squares[i].style.backgroundColor = color;
            }
        }
        else {
            // wrong
            square.style.backgroundColor = "#cceff3";
            messageContainer.textContent = "Wrong Choice!";
        }
    }
}

function renderSquares(colors: Array<string>, onClick: (square: HTMLDivElement, color: string) => void): Array<HTMLDivElement> {
    const squares = new Array<HTMLDivElement>()

    for (let i = 0; i < colors.length; i++) {
        const squareColor = colors[i]
        const square = document.createElement("div");
        squaresContainer.appendChild(square);
        square.setAttribute('style', `background-color: ${squareColor};`);
        square.addEventListener('click', () => { onClick(square, squareColor) });
        squares.push(square);
    }

    return squares
};

// initialization
function reset() {
    // reset interface
    aplicationState.won = false
    resetButton.textContent = "New Colors"
    messageContainer.textContent = "";

    // get selected number of squares
    aplicationState.numberOfSquares = Number(numberOfSquaresSelect.value)
    // generate colors based on it
    aplicationState.colors = generateRandomColors(aplicationState.numberOfSquares);
    // pick one target color
    aplicationState.targetColor = pickTargetColor(aplicationState.colors);
    // set target color to the title
    targetColorTitle.textContent = aplicationState.targetColor;
    // clear old squares
    for (let i = 0; i < aplicationState.squares.length; i++) {
        squaresContainer.removeChild(aplicationState.squares[i]);
    }
    // render squares
    aplicationState.squares = renderSquares(aplicationState.colors, handleSquareClick);
}

reset()

resetButton.addEventListener("click", reset)
numberOfSquaresSelect.addEventListener('change', reset);
